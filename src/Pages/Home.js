import { useContext, useEffect, useState } from "react";
import AuthContext from './../AuthContext';
import Parse from './../parse-config.js'
import { toBeDisabled } from "@testing-library/jest-dom/dist/matchers";

export default function Home() {

    const context = useContext(AuthContext);

    const [todoList, setTodoList] = useState([]);
    const [title, setTitle] = useState();
    const [adding, setAdding] = useState(false);
    const [editing, setEditing] = useState(false);

    const createTask = async () => {

        setAdding(true);

        const newTask = new Parse.Object('Todo');

        newTask.set('title',title);
        newTask.set('done',false);
        const relationOwner = newTask.relation('owner');
        relationOwner.add(context.user);

        try{
            const task = await newTask.save();

            setTodoList([...todoList,task]);

            setAdding(false);

            return true;
        }catch(e){
            setAdding(false);
            console.log(e.message);
            return false;
        }

    }

    const updateTask = async (row) => {

        setEditing(true);

        row.set('done',!row.get('done'));

        const task = new Parse.Object('Todo');

        task.set('objectId',row.id);
        task.set('done',row.get('done'));

        try{
            await task.save();

            setEditing(false);

            return true;
        }catch(e){
            console.log(e.message);

            setEditing(false);
            return false;
        }

    }

    const removeTask = async (item) => {

        setEditing(true);

        const task = new Parse.Object('Todo');

        task.set('objectId',item.id);

        try{
            await task.destroy();

            setTodoList(todoList.filter(row => (row.id!==item.id)));

            setEditing(false);

            return true;
        }catch(e){
            console.log(e.message);
            setEditing(false);
            return false;
        }

    }

    const loadTodoList = async () => {

        const query = new Parse.Query('Todo');
        query.equalTo('owner',context.user);

        try {
            const list = await query.find();
            
            setTodoList(list);

            return true;
        }catch(e){
            console.log(e.message);
            return false;
        }

    }

    useEffect(() => {
        loadTodoList();
    },[todoList]);

    return (
        <div className="card card-body mt-5">

            <div className="d-flex mb-3">
                <div className="flex-fill">
                </div>
                <div className="flex-fill text-end">
                    {context.user?.getUsername()}
                    <button type="button" className="btn btn-secondary ms-2" onClick={() => {context.onLogout()}}><i className="fa fa-power-off me-1"></i>Logout</button>
                </div>
            </div>
            
            <div className="card mb-4">
                <div className="card-header bg-success text-white">
                    Todo List
                    {editing===true && 
                        <span>
                            <i className="fa fa-spinner ms-2"></i>
                        </span>
                    }
                </div>
                <div className="card-body">

                    <div className="input-group mb-3">
                        <input type="text" className="form-control" placeholder="New task" onChange={(e) => {setTitle(e.target.value)}} />
                        <button className="btn btn-primary" disabled={adding===true} type="button" onClick={() => {createTask()}}>
                            <span>Create task</span>
                            {adding===true && 
                                <div>
                                    <i className="fa fa-spinner ms-2"></i>
                                </div>
                            }
                            </button>
                    </div>

                    <ul className="list-group">
                        {todoList.filter(row => (row.get('done')===false)).map(row => (
                            <li key={row.id} className="list-group-item d-flex">
                                <div className="flex-fill">
                                    {row.get('title')} 
                                    <input type="checkbox" className="form-check-input me-1 float-end" disabled={editing===true} onChange={() => {updateTask(row)}}/>
                                    <span className="float-end d-block me-2" role="button" onClick={() => {removeTask(row)}}><i className="fa fa-trash" ></i></span>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>

            <div className="card">
                <div className="card-header bg-warning text-white">
                    Done 
                    {editing===true && 
                        <span>
                            <i className="fa fa-spinner ms-2"></i>
                        </span>
                    }
                </div>
                <div className="card-body">
                    <ul className="list-group">
                        {todoList.filter(row => (row.get('done')===true)).map(row => (
                            <li key={row.id} className="list-group-item d-flex">
                                <div className="flex-fill">    
                                    {row.get('title')} 
                                    <input type="checkbox" className="form-check-input me-1 float-end" disabled={editing===true} checked onChange={() => {updateTask(row)}}/>
                                    <span className="float-end d-block me-2" role="button" onClick={() => {removeTask(row)}}><i className="fa fa-trash" ></i></span>
                                </div>
                            </li>
                        ))}
                    </ul>
                </div>
            </div>

        </div>        
    );

}