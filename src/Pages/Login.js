import { useState, useContext, useEffect } from "react";
import AuthContext from './../AuthContext';
import { useNavigate } from "react-router-dom";
import $, { nodeName } from 'jquery'

export default function Login() {

    const context = useContext(AuthContext);

    const navigate = useNavigate();

    const [loginEmail, setLoginEmail] = useState("");
    const [loginPassword, setLoginPassword] = useState("");
    const [registerEmail, setRegisterEmail] = useState("");
    const [registerPassword, setRegisterPassword] = useState("");

    useEffect(() => {
        context.onGetCurrentUser();
        if(context.user!=null){
            navigate("/");
        }
    },[context.user]);

    return (
        <div className="container mt-5">

            <div className="row">
                <div className="col-md-6 mb-3">
                    <div className="card">
                        <div className="card-header bg-primary text-white text-center">
                            Login
                        </div>
                        <div className="card-body">
                            <div className="mb-2">
                                <label>Email</label>
                                <input type="text" className="form-control" onChange={(e) => {setLoginEmail(e.target.value)}}/>
                            </div>  
                            <div className="mb-2">
                                <label>Password</label>
                                <input type="password" className="form-control" onChange={(e) => {setLoginPassword(e.target.value)}}/>
                            </div>  
                            <div className="text-center">
                                <button type="button" className="btn btn-primary" onClick={() => {context.onLogin(loginEmail, loginPassword)}}>Login</button>
                            </div>
                            <div id="loginError" style={{display:'none'}} className="alert alert-danger p-2 text-center" onClick={() => {$('#loginError').slideUp()}}>
                                Username or password is wrong
                            </div>
                        </div>
                    </div>
                </div>
            
                <div className="col-md-6 mb-3">
                    <div className="card">
                        <div className="card-header bg-success text-white text-center">
                            Register
                        </div>
                        <div className="card-body">
                            <div className="mb-2">
                                <label>Email</label>
                                <input type="text" className="form-control" onChange={(e) => {setRegisterEmail(e.target.value)}}/>
                            </div>  
                            <div className="mb-2">
                                <label>Password</label>
                                <input type="password" className="form-control" onChange={(e) => {setRegisterPassword(e.target.value)}}/>
                            </div>
                            <div className="text-center">
                                <button type="button" className="btn btn-success" onClick={() => {context.onRegister(registerEmail,registerPassword)}}>Register</button>
                            </div>
                            <div id="registerError" style={{display:'none'}} className="alert alert-danger p-2 text-center" onClick={() => {$('#registerError').slideUp()}}>
                                Username or password is wrong
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );

}