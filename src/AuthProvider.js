import { useState, useEffect } from "react";
import AuthContext from './AuthContext'
import { useNavigate } from "react-router-dom";
import Parse from './parse-config.js'
import $ from 'jquery'

const AuthProvider = ({children}) => {

    const [user, setUser] = useState(null);

    const navigate = useNavigate();

    //useEffect(() => {
    //    getCurrentUser();
    //},[]);

    // Function that will return current user and also update current username
    const getCurrentUser = async function () {
        const currentUser = await Parse.User.current();
        //console.log(currentUser);
        // Update state variable holding current user
        setUser(currentUser);
        return currentUser;
    };

    const register = async (email, password) => {
        try {
            // Since the signUp method returns a Promise, we need to call it using await
            const createdUser = await Parse.User.signUp(email, password);

            setUser(createdUser);

            navigate("/");

            console.log("user created");
            //alert(
            //  `Success! User ${createdUser.getUsername()} was successfully created!`
            //);

            return true;

        }catch(e){
            console.log(e.message);
        }
    } 

    const login = async (email, password) => {
        try {

            const loggedInUser = await Parse.User.logIn(email, password);
            // logIn returns the corresponding ParseUser object
            /*alert(
            `Success! User ${loggedInUser.get(
                'username'
            )} has successfully signed in!`
            );*/
            console.log("user signed in");
            // To verify that this is in fact the current user, `current` can be used
            const currentUser = await Parse.User.current();
            setUser(currentUser);

            navigate("/");
            //console.log(loggedInUser === currentUser);
            
            // Update state variable holding current user
            //getCurrentUser();
            return true;

        }catch(e){
            console.log(e.message);

            $('#loginError').slideDown();

            return false;
        }
    }

    const logout = async () => {
        

        try {
            await Parse.User.logOut();
            // To verify that current user is now empty, currentAsync can be used
            const currentUser = await Parse.User.current();
            setUser(null);
            //if (currentUser === null) {
            //  alert('Success! No user is logged in anymore!');
            //}
            // Update state variable holding current user
            getCurrentUser();
            navigate("/login");
            return true;
        } catch (error) {
            alert(`Error! ${error.message}`);
            $('#registerError').slideDown();
            return false;
        }

    }

    const params = {
        user,
        onLogin:login,
        onRegister:register,
        onLogout:logout,
        onGetCurrentUser:getCurrentUser
    };

    return (
        <AuthContext.Provider value={params}>
            {children}
        </AuthContext.Provider>
    );

}

export default AuthProvider;