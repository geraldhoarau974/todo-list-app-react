import { useContext } from "react";
import { Navigate } from "react-router-dom";
import AuthContext from './AuthContext'


export default function ProtectedRoute({children}) {

    const context = useContext(AuthContext);

    context.onGetCurrentUser();

    if(context.user==null){
        return <Navigate to="/login" replace></Navigate>
    }

    return children;

}